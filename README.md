# Conception - Application de gestion de budget

### User stories

Il s'agit d'une application de gestion de budget dans laquelle l'utilisateur pourra :
* Voir son solde et son solde prévisionnel
* Gestion du compte :
  * Ajouter, éditer, supprimer des entrées et des sorties
  * Voir les entrées et les sorties
  * Voir les entrées et les sorties par catégories 
  * Geler des entrées
  * Ajouter, éditer, supprimer des catégories
* Statistiques :
  * Voir les statistiques du compte
    * Par catégories
    * Mensuelles
    * Annuelles
  * Ajouter, éditer, supprimer des entrées et des sorties via les statistiques

### Classes

* User <br />
  * Propriétés :
    * id
    * surname
    * lastname
  * Méthodes :
    * 
      <br />
      <br />
* Account <br />
  * Propriétés :
    * id
    * balance
    * forecast balance
    * user (foreign key)
  * Méthodes :
    *
    <br />
    <br />
* Entry <br />
  * Propriétés :
    * id
    * name
    * ammount
    * account (foreign key)
  * Méthodes :
    * 
    <br />
    <br />
* Output <br />
  * Propriétés :
    * id
    * name
    * ammount
    * account (foreign key)
  * Méthodes :
    * 
    <br />
    <br />
* Category <br />
  * Propriétés :
    * id
    * name
  * Méthodes :
    * 

Toutes les propriétés sont privées pour des raisons de sécurité. Se sont les getters et les setters de mes classes qui me permettront de manipuler les différentes propriétés si besoin.
Les méthodes sont publique pour que nous puissons y avoir accès n'importe ou dans l'application en instanciant la classe en question.